﻿(*
 * Big Integer Utility
 *
 * PLATFORMS
 *   Windows / macOS / Android / iOS
 *
 * ENVIRONMENT
 *   Delphi 10.3.x or lator
 *
 * LICENSE
 *   Copyright (C) 2020 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2005/08/21 Version 1.0.0  First Release
 *   2017/06/08 Version 1.1.0  Define operator overload
 *   2020/11/28 Version 2.0.0  Change class to record.
 *
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit PK.Math.BigInteger;

interface

uses
  System.Contnrs;

type
  TBigIntegerDigits = array of Cardinal;

  TBigInteger = record
  private var
    // Variables
    FDigits: TBigIntegerDigits;
    FSign: Integer;
    FBase: Cardinal;
    FMaxValue: Cardinal;
    FBaseBitCount: Cardinal;
    FBaseIsOdd: Integer;
    FDispUnit: Cardinal;
    FDispUnitLen: Integer;
    FPrefixZero: String;
  private
    // Property Implementation of Methods
    function GetIsZero: Boolean;
    function GetIsOdd: Boolean;
    function GetDigits(const iIndex: Integer): Cardinal;
    procedure SetDigits(const iIndex: Integer; iValue: Cardinal);
    function GetDigitCount: Cardinal;
    // Methods
    procedure SetSign(const iSign: Integer);
    procedure Adjust;
    function NewSameBase(
      const iBigInteger: TBigInteger;
      const iForceNew: Boolean = False): TBigInteger;
    procedure InternalSetDigits(const iDigits: TBigIntegerDigits);
    function InternalGetValidLength(const iDigits: TBigIntegerDigits): Integer;
    function GetValidLength: Integer;
    function Expand(const iCount: Integer): Integer;
    function CloneDigits: TBigIntegerDigits;
    function CompareLength(const iNum: TBigInteger): Integer;
    function InternalCompare(const iNum: TBigInteger): Integer;
    procedure InternalAdd(const iNum: TBigInteger);
    procedure InternalSub(const iNum: TBigInteger);
    procedure InternalDiv(const iNum, iQuotient, iRemainder: TBigInteger);
  public
    // Constructor
    constructor Create(const iBase: Cardinal);
    constructor CreateInteger(
      const iInteger: Integer;
      const iBase: Cardinal = 0);
    constructor CreateBigInteger(
      const iBigInteger: TBigInteger;
      const iBase: Cardinal = 0);
    constructor CreateString(iString: String; const iBase: Cardinal = 0);
    // Methods
    function Add(const iNum: TBigInteger): TBigInteger; overload;
    function Add(const iNum: Integer): TBigInteger; overload;
    function Sub(const iNum: TBigInteger): TBigInteger; overload;
    function Sub(const iNum: Integer): TBigInteger; overload;
    function Mul(const iNum: TBigInteger): TBigInteger; overload;
    function Mul(const iNum: Integer): TBigInteger; overload;
    function Dv(const iNum: TBigInteger): TBigInteger; overload;
    function Dv(const iNum: Integer): TBigInteger; overload;
    function Md(const iNum: TBigInteger): TBigInteger; overload;
    function Md(const iNum: Integer): TBigInteger; overload;
    function Pow(const iNum: TBigInteger): TBigInteger; overload;
    function Pow(const iNum: Cardinal): TBigInteger; overload;
    function ModPow(
      const iPow, iMod: TBigInteger): TBigInteger; overload;
    function ModPow(
      const iPow: Cardinal;
      const iMod: TBigInteger): TBigInteger; overload;
    function ModPow(
      const iPow: Cardinal;
      const iMod: Integer): TBigInteger; overload;
    function ModPow(
      const iPow: TBigInteger;
      const iMod: Integer): TBigInteger; overload;
    function ShiftL(const iNum: Cardinal): TBigInteger;
    function ShiftR(const iNum: Cardinal): TBigInteger;
    function PushBit(const iInBit: Cardinal): TBigInteger;
    procedure AssignZero;
    procedure AssignInteger(iInteger: Integer);
    procedure AssignString(iString: String);
    procedure Assign(const iBigInteger: TBigInteger);
    function Compare(const iNum: TBigInteger): Integer; overload;
    function Compare(const iNum: Integer): Integer; overload;
    function Equal(const iNum: TBigInteger): Boolean; overload;
    function Equal(const iNum: Integer): Boolean; overload;
    function AsString: String;
    // Operater overload
    class operator Add(iA, iB: TBigInteger): TBigInteger; overload;
    class operator Add(iA: TBigInteger; iB: Integer): TBigInteger; overload;
    class operator Subtract(iA, iB: TBigInteger): TBigInteger; overload;
    class operator Subtract(
      iA: TBigInteger;
      iB: Integer): TBigInteger; overload;
    class operator Multiply(iA, iB: TBigInteger): TBigInteger; overload;
    class operator Multiply(
      iA: TBigInteger;
      iB: Integer): TBigInteger; overload;
    class operator IntDivide(iA, iB: TBigInteger): TBigInteger; overload;
    class operator IntDivide(
      iA: TBigInteger;
      iB: Integer): TBigInteger; overload;
    class operator Modulus(iA, iB: TBigInteger): TBigInteger; overload;
    class operator Modulus(iA: TBigInteger; iB: Integer): TBigInteger; overload;
    class operator LeftShift(iA: TBigInteger; iB: Cardinal): TBigInteger;
    class operator RightShift(iA: TBigInteger; iB: Cardinal): TBigInteger;
    class operator Equal(iA, iB: TBigInteger): Boolean; overload;
    class operator NotEqual(iA, iB: TBigInteger): Boolean; overload;
    class operator GreaterThan(iA, iB: TBigInteger): Boolean; overload;
    class operator GreaterThanOrEqual(iA, iB: TBigInteger): Boolean; overload;
    class operator LessThan(iA, iB: TBigInteger): Boolean; overload;
    class operator LessThanOrEqual(iA, iB: TBigInteger): Boolean; overload;
    class operator Implicit(iA: TBigInteger): String; overload;
    class operator Implicit(iA: String): TBigInteger; overload;
    class operator Implicit(iA: Integer): TBigInteger; overload;
    class operator Explicit(iA: TBigInteger): String; overload;
    class operator Explicit(iA: String): TBigInteger; overload;
    class operator Explicit(iA: Integer): TBigInteger; overload;
    // Property
    property IsZero: Boolean read GetIsZero;
    property IsOdd: Boolean read GetIsOdd;
    property Sign: Integer read FSign;
    property DigitCount: Cardinal read GetDigitCount;
    property Digits[const iIndex: Integer]: Cardinal
      read GetDigits write SetDigits; default;
  end;

implementation

uses
  System.SysUtils
  , System.Math
  ;

const
  CBase = High(Word) div 2; // = 2 ^ 15 - 1 = $7fff (Word = 16bit)

{ TBigInteger }

(*
 * 概要  加算
 * 引数  iNum  加える値
 * 戻値  自分のコピー
 *)
function TBigInteger.Add(const iNum: TBigInteger): TBigInteger;
begin
  if (FSign = iNum.FSign) then
    InternalAdd(iNum)
  else
  begin
    if (CompareLength(iNum) < 0) then
      FSign := iNum.FSign;

    InternalSub(iNum);
  end;

  Result := Self;
end;

(*
 * 概要  加算
 * 引数  iNum  加える値
 * 戻値  自分のコピー
 *)
function TBigInteger.Add(const iNum: Integer): TBigInteger;
begin
  Result := Add(TBigInteger.CreateInteger(iNum, FBase));
end;

class operator TBigInteger.Add(iA, iB: TBigInteger): TBigInteger;
begin
  Result := iA.Add(iB);
end;

class operator TBigInteger.Add(iA: TBigInteger; iB: Integer): TBigInteger;
begin
  Result := iA.Add(iB);
end;

(*
 * 概要  配列の不要部分を削除
 *)
procedure TBigInteger.Adjust;
begin
  var Count := GetValidLength;

  if (Count = 0) then
    AssignZero
  else
    SetLength(FDigits, Count);
end;

(*
 * 概要  値を iBigInteger と同じにする
 * 引数  iBigInteger  設定する値
 *)
procedure TBigInteger.Assign(const iBigInteger: TBigInteger);
var
  DstDigits, tmpDigits, Carrys: TBigIntegerDigits;
  Value: Cardinal;
begin
  FSign := iBigInteger.FSign;

  if (FBase = iBigInteger.FBase) then
    // 基数が同じ
    InternalSetDigits(iBigInteger.FDigits)
  else
  begin
    // 基数が違う
    var SrcLen := Length(iBigInteger.FDigits);
    var DstLen := Trunc(SrcLen * LogN(FBase, iBigInteger.FBase)) + 1;

    SetLength(DstDigits, DstLen);
    SetLength(tmpDigits, DstLen);
    SetLength(Carrys, DstLen);

    for var i := 0 to DstLen - 1 do
      DstDigits[i] := 0;

    for var i := 0 to SrcLen - 1 do
    begin
      for var j := 0 to DstLen - 1 do
        tmpDigits[j] := 0;

      tmpDigits[0] := iBigInteger.FDigits[i];

      for var j := 0 to i - 1 do
      begin
        for var k := 0 to DstLen - 1 do
          Carrys[k] := 0;

        for var k := 0 to DstLen - 1 do
        begin
          Value := tmpDigits[k] * iBigInteger.FBase;

          tmpDigits[k] := Value mod FBase;

          if (k < (DstLen - 1)) then
            Carrys[k + 1] := Value div FBase;
        end;

        var Carry: Cardinal := 0;
        for var k := 0 to DstLen - 1 do
        begin
          Value := tmpDigits[k] + Carrys[k] + Carry;

          tmpDigits[k] := Value mod FBase;
          Carry := Value div FBase;
        end;
      end;

      for var j := 0 to DstLen - 1 do
      begin
        Value := DstDigits[j] + tmpDigits[j];

        DstDigits[j] := Value mod FBase;

        if (j < (DstLen - 1)) then
          Inc(DstDigits[j + 1], Value div FBase);
      end;
    end;

    InternalSetDigits(DstDigits);
  end;
end;

(*
 * 概要  値を iInteger と同じにする
 * 引数  iInteger  設定する値
 *)
procedure TBigInteger.AssignInteger(iInteger: Integer);
begin
  if (iInteger < 0) then
    FSign := -1
  else
    FSign := +1;

  iInteger := Abs(iInteger);

  var Len := 0;
  var tmpInt := iInteger;
  while (tmpInt > 0) do
  begin
    tmpInt := tmpInt div Integer(FBase);
    Inc(Len);
  end;

  if (Len = 0) then
    Len := 1;

  SetLength(FDigits, Len);

  for var i := 0 to Len - 1 do
  begin
    tmpInt := iInteger mod Integer(FBase);

    FDigits[i] := tmpInt;

    iInteger := iInteger div Integer(FBase);
  end;
end;

(*
 * 概要  値を vString と同じにする
 * 引数  iString  初期値
 *)
procedure TBigInteger.AssignString(iString: String);
var
  Value: Integer;
begin
  var Decimal := TBigInteger.Create(10);
  var Len := Length(iString);

  if (Len > 0) then
  begin
    if CharInSet(iString[1], ['-', '+']) then
    begin
      Decimal.FSign := StrToIntDef(iString[1] + '1', 1);

      Delete(iString, 1, 1);
      Dec(Len);
    end;

    SetLength(Decimal.FDigits, Len);

    for var i := 0 to Len - 1 do
    begin
      Value := StrToIntDef(iString[Len - i], -1);

      if (Value > -1) then
        Decimal.FDigits[i] := Value;
    end;

    Assign(Decimal);
  end;
end;

(*
 * 概要  値をゼロにする
 *)
procedure TBigInteger.AssignZero;
begin
  SetLength(FDigits, 1);
  FDigits[0] := 0;
  FSign := +1;
end;

(*
 * 概要  数値の文字列表現を返す
 * 戻値  数値の文字列表現
 *)
function TBigInteger.AsString: String;
var
  Len: Integer;
  tmpDigits: TBigIntegerDigits;
  tmpDigitsLen: Integer;
  Value: Cardinal;
  tmpResult: String;
  tmpResultLen: Integer;
  ResultStack: String;

  function GetValue(const iIndex: Integer; const vShift: Integer): Cardinal;
  var
    tmpValue: Cardinal;
    tmpCarry: Cardinal;
  begin
    Result := 0;

    if (iIndex >= Len) then
      Exit;

    for var j := 0 to tmpDigitsLen - 1 do
      tmpDigits[j] := 0;

    tmpDigits[0] := FDigits[iIndex];

    for var i := 0 to iIndex - 1 do
    begin
      tmpCarry := 0;

      for var j := 0 to tmpDigitsLen - 1 do
      begin
        tmpValue := tmpDigits[j] * FBase + tmpCarry;

        tmpCarry := tmpValue div FDispUnit;
        tmpDigits[j] := tmpValue mod FDispUnit;
      end;
    end;

    Result := tmpDigits[tmpDigitsLen - 1];
  end;

begin
  Result := '';

  Len := Length(FDigits);

  var Carry: Cardinal := 0;

  for var i := 0 to Len - 1 do
  begin
    SetLength(tmpDigits, i + 1);
    tmpDigitsLen := Length(tmpDigits);

    Value := GetValue(i, i) + Carry;

    for var j := i + 1 to Len - 1 do
      Value := Value + GetValue(j, i);

    Carry := Value div FDispUnit;
    Value := Value mod FDispUnit;

    tmpResult := FPrefixZero + IntToStr(Value);
    tmpResultLen := Length(tmpResult);

    ResultStack :=
      Copy(tmpResult, tmpResultLen - FDispUnitLen + 1, FDispUnitLen) +
      ResultStack;

    if (Value <> 0) then
    begin
      Result := ResultStack + Result;
      ResultStack := '';
    end;
  end;

  while (Copy(Result, 1, 1) = '0') do
    Result := Copy(Result, 2, Length(Result));

  if (Result = '') then
    Result := '0'
  else
  begin
    if (FSign < 0) then
      Result := '-' + Result;
  end;
end;

(*
 * 概要  FDigits を複製して返す
 * 戻値  複製された FDigits
 *)
function TBigInteger.CloneDigits: TBigIntegerDigits;
begin
  var Len := Length(FDigits);

  SetLength(Result, Len);

  for var i := 0 to Len - 1 do
    Result[i] := FDigits[i];
end;

(*
 * 概要  比較する
 * 引数  iNum  比較する値
 * 戻値  Self > iNum の場合 +1
 *       Self = iNum の場合  0
 *       Self < iNum の場合 -1
 *)
function TBigInteger.Compare(const iNum: TBigInteger): Integer;
begin
  Result := InternalCompare(NewSameBase(iNum));
end;

(*
 * 概要  比較する
 * 引数  iNum  比較する値
 * 戻値  Self > iNum の場合 +1
 *       Self = iNum の場合  0
 *       Self < iNum の場合 -1
 *)
function TBigInteger.Compare(const iNum: Integer): Integer;
begin
  Result := Compare(TBigInteger.CreateInteger(iNum, FBase));
end;

(*
 * 概要  比較する
 * 引数  iNum  比較する値
 * 戻値  Self > iNum の場合 +1
 *       Self = iNum の場合  0
 *       Self < iNum の場合 -1
 *)
function TBigInteger.CompareLength(const iNum: TBigInteger): Integer;
begin
  Result := 0;

  var SrcLen := GetValidLength;
  var DstLen := iNum.GetValidLength;

  if (SrcLen > DstLen) then
    Result := +1
  else
    if (SrcLen < DstLen) then
      Result := -1
    else
    begin
      for var i := SrcLen - 1 downto 0 do
      begin
        if (FDigits[i] > iNum.FDigits[i]) then
        begin
          Result := +1;
          Break;
        end
        else if (FDigits[i] < iNum.FDigits[i]) then
        begin
          Result := -1;
          Break;
        end;
      end;
    end;
end;

(*
 * 概要  コンストラクタ
 * 引数  iBase  基数
 * 備考  1 < iBase <= CBase
 *)
constructor TBigInteger.Create(const iBase: Cardinal);
begin
  FSign := +1;
  FBase := iBase;

  if (FBase < 2) or (FBase > CBase) then
    FBase := CBase;

  FMaxValue := FBase - 1;

  FBaseIsOdd := FBase and 1;
  FBaseBitCount := Trunc(Log2(FBase)) + FBaseIsOdd;

  FDispUnit := 1;
  FDispUnitLen := 0;
  while (FBase > FDispUnit) do
  begin
    FDispUnit := FDispUnit * 10;
    Inc(FDispUnitLen);
  end;

  FPrefixZero := StringOfChar('0', FDispUnitLen);

  AssignZero;
end;

(*
 * 概要  コンストラクタ
 * 引数  iBigInteger  初期値
 *       iBase        基数
 *)
constructor TBigInteger.CreateBigInteger(
  const iBigInteger: TBigInteger;
  const iBase: Cardinal);
begin
  Create(iBase);

  Assign(iBigInteger);
end;

(*
 * 概要  コンストラクタ
 * 引数  iInteger  初期値
 *       iBase     基数
 *)
constructor TBigInteger.CreateInteger(
  const iInteger: Integer;
  const iBase: Cardinal);
begin
  Create(iBase);

  AssignInteger(iInteger);
end;

(*
 * 概要  コンストラクタ
 * 引数  iString  初期値
 *       iBase    基数
 *)
constructor TBigInteger.CreateString(
  iString: String;
  const iBase: Cardinal);
begin
  Create(iBase);

  AssignString(iString);
end;

(*
 * 概要  除算
 * 引数  iNum  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.Dv(const iNum: TBigInteger): TBigInteger;
begin
  var Quotient := TBigInteger.CreateBigInteger(Self, FBase);
  var Remainder := TBigInteger.Create(FBase);

  InternalDiv(iNum, Quotient, Remainder);

  Assign(Quotient);

  Result := Self;
end;

(*
 * 概要  除算
 * 引数  iNum  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.Dv(const iNum: Integer): TBigInteger;
begin
  Result := Dv(TBigInteger.CreateInteger(iNum, FBase));
end;

(*
 * 概要  iNum と同じかどうかを返す
 * 引数  iNum  検査対象
 * 戻値  同じなら True
 *)
function TBigInteger.Equal(const iNum: TBigInteger): Boolean;
begin
  Result := (Compare(iNum) = 0);
end;

(*
 * 概要  iNum と同じかどうかを返す
 * 引数  iNum  検査対象
 * 戻値  同じなら True
 *)
function TBigInteger.Equal(const iNum: Integer): Boolean;
begin
  Result := (Compare(iNum) = 0);
end;

class operator TBigInteger.Equal(iA, iB: TBigInteger): Boolean;
begin
  Result := iA.Equal(iB);
end;

(*
 * 概要  桁数を増やす
 * 引数  iCount  増やす桁数
 * 戻値  現在の桁数
 *)
function TBigInteger.Expand(const iCount: Integer): Integer;
begin
  var Len := Length(FDigits);
  SetLength(FDigits, Len + iCount);

  Result := Length(FDigits);

  for var i := Len to Result - 1 do
    FDigits[i] := 0;
end;

class operator TBigInteger.Explicit(iA: TBigInteger): String;
begin
  Result := iA.AsString;
end;

class operator TBigInteger.Explicit(iA: String): TBigInteger;
begin
  Result := TBigInteger.CreateString(iA);
end;

class operator TBigInteger.Explicit(iA: Integer): TBigInteger;
begin
  Result := TBigInteger.CreateInteger(iA);
end;

(*
 * 概要  桁数を返す
 * 戻値  桁数
 *)
function TBigInteger.GetDigitCount: Cardinal;
begin
  Result := 0;

  for var i := Length(FDigits) - 1 downto 0 do
    if (FDigits[i] <> 0) then
    begin
      Result := i + 1;
      Break;
    end;
end;

(*
 * 概要  各桁の値を返す
 * 引数  iIndex  桁
 * 戻値  値
 *)
function TBigInteger.GetDigits(const iIndex: Integer): Cardinal;
begin
  Result := 0;

  if (iIndex > -1) and (iIndex < Length(FDigits)) then
    Result := FDigits[iIndex];
end;

(*
 * 概要  奇数かどうかを返す
 * 戻値  奇数なら True
 *)
function TBigInteger.GetIsOdd: Boolean;
begin
  var Len := Length(FDigits);

  if (Len < 1) then
    Exit(False);

  var Value: Cardinal := 0;
  for var i := 1 to Len - 1 do
    Value := (Value xor FDigits[i]) and FBaseIsOdd;

  Result := ((FDigits[0] + Value) and 1 > 0);
end;

(*
 * 概要  ゼロかどうかを返す
 * 戻値  ゼロなら True
 *)
function TBigInteger.GetIsZero: Boolean;
begin
  Result := True;

  for var i := 0 to Length(FDigits) - 1 do
    if (FDigits[i] <> 0) then
    begin
      Result := False;
      Break;
    end;
end;

(*
 * 概要  有効な配列長を返す
 * 戻値  有効な配列長
 *)
function TBigInteger.GetValidLength: Integer;
begin
  Result := InternalGetValidLength(FDigits);
end;

class operator TBigInteger.GreaterThan(iA, iB: TBigInteger): Boolean;
begin
  Result := iA.Compare(iB) > 0;
end;

class operator TBigInteger.GreaterThanOrEqual(iA, iB: TBigInteger): Boolean;
begin
  Result := iA.Compare(iB) >= 0;
end;

class operator TBigInteger.Implicit(iA: TBigInteger): String;
begin
  Result := iA.AsString;
end;

class operator TBigInteger.Implicit(iA: String): TBigInteger;
begin
  Result := TBigInteger.CreateString(iA);
end;

class operator TBigInteger.Implicit(iA: Integer): TBigInteger;
begin
  Result := TBigInteger.CreateInteger(iA);
end;

class operator TBigInteger.IntDivide(iA: TBigInteger; iB: Integer): TBigInteger;
begin
  Result := iA.Dv(iB);
end;

class operator TBigInteger.IntDivide(iA, iB: TBigInteger): TBigInteger;
begin
  Result := iA.Dv(iB);
end;

(*
 * 概要  加算（単純に配列を足す）
 * 引数  iNum  加える値
 *)
procedure TBigInteger.InternalAdd(const iNum: TBigInteger);
var
  Value: Cardinal;
begin
  var Num := NewSameBase(iNum);
  var Len := Length(Num.FDigits);
  var SrcLen := Length(FDigits);

  if (Len > SrcLen) then
    Len := Expand(Len - SrcLen);

  var Carry: Cardinal := 0;
  for var i := 0 to Len - 1 do
  begin
    Value := FDigits[i] + Num.FDigits[i] + Carry;

    Carry := Value div FBase;
    FDigits[i] := Value mod FBase;
  end;

  if (Carry > 0) then
  begin
    Expand(1);

    var CarryNum := TBigInteger.Create(FBase);
    CarryNum.Expand(Len + 1);
    CarryNum.FDigits[Len] := Carry;

    InternalAdd(CarryNum);
  end;

  Adjust;
end;

(*
 * 概要  比較する
 * 引数  iNum  比較する値
 * 戻値  Self > iNum の場合 +1
 *       Self = iNum の場合  0
 *       Self < iNum の場合 -1
 *)
function TBigInteger.InternalCompare(const iNum: TBigInteger): Integer;
begin
  if (FSign > iNum.FSign) then
    Result := +1
  else
    if (FSign < iNum.FSign) then
      Result := -1
    else
      Result := CompareLength(iNum);
end;

(*
 * 概要  除算する
 * 引数  iNum        割る数
 *       iQuotient   商を格納する BigInteger
 *       iRemainder  余りを格納する BigInteger
 *)
procedure TBigInteger.InternalDiv(
  const iNum, iQuotient, iRemainder: TBigInteger);
begin
  if (iNum.IsZero) then
    Exit;

  iQuotient.AssignZero;
  iRemainder.Assign(Self);

  if (CompareLength(iNum) < 0) then
    Exit;

  var Q := NewSameBase(iNum, True);
  Q.FSign := 1;
  iRemainder.SetSign(+1);

  var QLen := Length(Q.FDigits);
  var RLen := Length(iRemainder.FDigits);

  if (QLen <= RLen) then
    Q.ShiftL(Cardinal(RLen - QLen + 1) * (Q.FBaseBitCount + 1));

  while (Q.InternalCompare(iNum) >= 0) do
  begin
    if (iRemainder.InternalCompare(Q) < 0) then
      iQuotient.PushBit(0)
    else
    begin
      iRemainder.InternalSub(Q);
      iQuotient.PushBit(1);
    end;

    Q.ShiftR(1);
  end;

  if (FSign = iNum.FSign) then
    iQuotient.SetSign(+1)
  else
    iQuotient.SetSign(-1);

  Adjust;
end;

(*
 * 概要  有効な配列長を返す
 * 引数  iDigits  検査する配列
 * 戻値  有効な配列長
 *)
function TBigInteger.InternalGetValidLength(
  const iDigits: TBigIntegerDigits): Integer;
begin
  Result := 0;

  for var i := 0 to Length(iDigits) - 1 do
    if (iDigits[i] <> 0) then
      Result := i + 1;
end;

(*
 * 概要  FDigits を iDigits と同じ値にする
 * 引数  iDigits  設定する値
 *)
procedure TBigInteger.InternalSetDigits(const iDigits: TBigIntegerDigits);
begin
  var Count := InternalGetValidLength(iDigits);

  if (Count = 0) then
    AssignZero
  else
  begin
    SetLength(FDigits, Count);

    for var i := 0 to Count - 1 do
      FDigits[i] := iDigits[i];
  end;
end;

(*
 * 概要  減算（単純に配列から引く）
 * 引数  iNum  減じる値
 *)
procedure TBigInteger.InternalSub(const iNum: TBigInteger);
var
  Term1Len: Integer;
  Term1, Term2: TBigIntegerDigits;
  Len: Integer;
  i, j: Integer;
  Value1, Value2: Cardinal;
  Index: Integer;
  SIndex: Integer;
begin
  var Num := NewSameBase(iNum);
  var Len1 := Length(FDigits);
  var Len2 := Length(Num.FDigits);

  if (CompareLength(Num) < 0) then
  begin
    Len := Len1;
    Term1Len := Len2;
    Term1 := Num.CloneDigits;
    Term2 := CloneDigits;
  end
  else
  begin
    Len := Len2;
    Term1Len := Len1;
    Term1 := CloneDigits;
    Term2 := Num.CloneDigits;
  end;

  for i := 0 to Len - 1 do
  begin
    Value1 := Term1[i];
    Value2 := Term2[i];

    if (Value1 > Value2) then
      Dec(Value1, Value2)
    else
    if (Value1 < Value2) then
    begin
      Value1 := Value1 + FBase - Value2;

      SIndex := i + 1;

      if (SIndex < Term1Len) then
      begin
        Index := SIndex;

        while (Term1[Index] < 1) do
          Inc(Index);

        for j := SIndex to Index - 1 do
          Term1[j] := FMaxValue;

        Dec(Term1[Index]);
      end;
    end
    else
      Value1 := 0;

    Term1[i] := Value1;
  end;

  InternalSetDigits(Term1);
end;

class operator TBigInteger.LeftShift(
  iA: TBigInteger;
  iB: Cardinal): TBigInteger;
begin
  Result := iA.ShiftL(iB);
end;

class operator TBigInteger.LessThan(iA, iB: TBigInteger): Boolean;
begin
  Result := iA.Compare(iB) < 0;
end;

class operator TBigInteger.LessThanOrEqual(iA, iB: TBigInteger): Boolean;
begin
  Result := iA.Compare(iB) <= 0;
end;

(*
 * 概要  剰余を求める
 * 引数  iNum  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.Md(const iNum: TBigInteger): TBigInteger;
begin
  var Quotient := TBigInteger.CreateBigInteger(Self, FBase);
  var Remainder := TBigInteger.Create(FBase);

  InternalDiv(iNum, Quotient, Remainder);

  Assign(Remainder);

  Result := Self;
end;

(*
 * 概要  剰余を求める
 * 引数  iNum  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.Md(const iNum: Integer): TBigInteger;
begin
  Result := Md(TBigInteger.CreateInteger(iNum, FBase));
end;

(*
 * 概要  階乗を求めて剰余を求める
 * 引数  iPow  乗数
 *       iMod  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.ModPow(
  const iPow, iMod: TBigInteger): TBigInteger;
begin
  if (iPow.FSign < 0) then
  begin
    AssignInteger(1);
    Exit(Self);
  end;

  var ShiftPow := TBigInteger.CreateBigInteger(iPow, FBase);
  var ModFact := TBigInteger.CreateBigInteger(iMod, FBase);
  var Value := TBigInteger.CreateInteger(1, FBase);
  var Own := TBigInteger.CreateBigInteger(Self, FBase);
  var tmpOwn := TBigInteger.Create(FBase);

  while (not ShiftPow.IsZero) do
  begin
    if (ShiftPow.GetIsOdd) then
    begin
      Value.Mul(Own);
      Value.Md(ModFact);
    end;

    tmpOwn.Assign(Own);

    Own.Mul(tmpOwn);
    Own.Md(ModFact);

    ShiftPow.ShiftR(1);
  end;

  Assign(Value);
  Result := Self;
end;

(*
 * 概要  階乗を求めて剰余を求める
 * 引数  iPow  乗数
 *       iMod  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.ModPow(
  const iPow: Cardinal;
  const iMod: TBigInteger): TBigInteger;
begin
  Result := ModPow(TBigInteger.CreateInteger(iPow), iMod);
end;

(*
 * 概要  階乗を求めて剰余を求める
 * 引数  iPow  乗数
 *       iMod  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.ModPow(
  const iPow: TBigInteger;
  const iMod: Integer): TBigInteger;
begin
  Result := ModPow(iPow, TBigInteger.CreateInteger(iMod));
end;

class operator TBigInteger.Modulus(iA: TBigInteger; iB: Integer): TBigInteger;
begin
  Result := iA.Md(iB);
end;

class operator TBigInteger.Modulus(iA, iB: TBigInteger): TBigInteger;
begin
  Result := iA.Md(iB);
end;

(*
 * 概要  階乗を求めて剰余を求める
 * 引数  iPow  乗数
 *       iMod  割る値
 * 戻値  自分のコピー
 *)
function TBigInteger.ModPow(
  const iPow: Cardinal;
  const iMod: Integer): TBigInteger;
begin
  Result :=
    ModPow(TBigInteger.CreateInteger(iPow), TBigInteger.CreateInteger(iMod));
end;

(*
 * 概要  乗算
 * 引数  iNum  掛ける値
 * 戻値  自分のコピー
 *)
function TBigInteger.Mul(const iNum: TBigInteger): TBigInteger;
var
  Carry: Cardinal;
  Value: Cardinal;
  Term: Cardinal;
begin
  var Num := NewSameBase(iNum);
  FSign := FSign * iNum.FSign;

  var OrgLen := Length(FDigits);
  var DstLen := Length(Num.FDigits);

  var Src := TBigInteger.CreateBigInteger(Self, FBase);
  var Index := OrgLen * DstLen + 1;
  Expand(Index);
  Src.Expand(Index);

  var SrcLen := Length(Src.FDigits);

  for var i := 0 to SrcLen - 1 do
    FDigits[i] := 0;

  for var i := 0 to DstLen - 1 do
  begin
    Term := Num.FDigits[i];

    for var j := 0 to OrgLen - 1 do
    begin
      Index := j + i;
      if (Index >= SrcLen) then
        Break;

      Value := FDigits[Index] + Src[j] * Term;

      Carry := Value div FBase;
      FDigits[Index] := Value mod FBase;

      Inc(Index);
      while (Carry > 0) and (Index < (SrcLen - 1)) do
      begin
        Value := FDigits[Index] + Carry;

        Carry := Value div FBase;
        FDigits[Index] := Value mod FBase;

        Inc(Index);
      end;
    end;
  end;

  Adjust;

  Result := Self;
end;

(*
 * 概要  乗算
 * 引数  iNum  掛ける値
 * 戻値  自分のコピー
 *)
function TBigInteger.Mul(const iNum: Integer): TBigInteger;
begin
  Result := Mul(TBigInteger.CreateInteger(iNum, FBase));
end;

class operator TBigInteger.Multiply(iA, iB: TBigInteger): TBigInteger;
begin
  Result := iA.Mul(iB);
end;

class operator TBigInteger.Multiply(iA: TBigInteger; iB: Integer): TBigInteger;
begin
  Result := iA.Mul(iB);
end;

(*
 * 概要  iBigInteger の基数が同じなら、iBigInteger を返す
 *       基数が違うなら基数を同じにした新しいインスタンスを返す
 * 引数  iBigInteger  値が欲しい BigInteger のインスタンス
 *       iForceNew    新しいインスタンスを強制的に作成する
 * 戻値  iBigInteger or New Instance
 *)
function TBigInteger.NewSameBase(
  const iBigInteger: TBigInteger;
  const iForceNew: Boolean): TBigInteger;
begin
  Result := iBigInteger;

  if (iBigInteger.FBase <> FBase) or (iForceNew) then
    Result := TBigInteger.CreateBigInteger(iBigInteger, FBase);
end;

class operator TBigInteger.NotEqual(iA, iB: TBigInteger): Boolean;
begin
  Result := not iA.Equal(iB);
end;

(*
 * 概要  階乗を求める
 * 引数  iNum  乗数
 * 戻値  自分のコピー
 *)
function TBigInteger.Pow(const iNum: Cardinal): TBigInteger;
begin
  var BI := TBigInteger.CreateBigInteger(Self, FBase);
  for var i := 1 to iNum do
    Mul(BI);

  Result := Self;
end;

(*
 * 概要  階乗を求める
 * 引数  iNum  乗数
 * 戻値  自分のコピー
 *)
function TBigInteger.Pow(const iNum: TBigInteger): TBigInteger;
begin
  var BI := TBigInteger.CreateBigInteger(Self, FBase);
  var Num := TBigInteger.CreateBigInteger(iNum);

  while (not Num.IsZero) do
  begin
    Mul(BI);
    Num.Sub(1);
  end;

  Result := Self;
end;

(*
 * 概要  左に １個シフトして vInBit を LSB にする
 * 引数  iInBit  LSB の値( 0 or 1 )
 * 戻値  自分のコピー
 *)
function TBigInteger.PushBit(const iInBit: Cardinal): TBigInteger;
begin
  ShiftL(1);

  if (Length(FDigits) > 0) then
    FDigits[0] := FDigits[0] + (iInBit and 1);

  Result := Self;
end;

class operator TBigInteger.RightShift(
  iA: TBigInteger;
  iB: Cardinal): TBigInteger;
begin
  Result := iA.ShiftR(iB);
end;

(*
 * 概要  各桁の値を設定する
 * 引数  iIndex  桁
 *       iValue   値
 *)
procedure TBigInteger.SetDigits(const iIndex: Integer; iValue: Cardinal);
begin
  if (iIndex > -1) and (iIndex < Length(FDigits)) then
  begin
    if (iValue > FMaxValue) then
      iValue := FMaxValue;

    FDigits[iIndex] := iValue;
  end;
end;

procedure TBigInteger.SetSign(const iSign: Integer);
begin
  FSign := iSign;
end;

(*
 * 概要  左に iNum 個シフトする
 * 引数  iNum  シフトするビット数
 * 戻値  自分のコピー
 *)
function TBigInteger.ShiftL(const iNum: Cardinal): TBigInteger;
var
  Value: Cardinal;
begin
  var Carry := FBaseBitCount - 1;
  if (Carry < 1) then
    Carry := 1;

  Expand(iNum div Carry + 1);
  var Len := Length(FDigits);

  for var i := 0 to iNum - 1 do
  begin
    Carry := 0;
    for var j := 0 to Len - 1 do
    begin
      Value := (FDigits[j] shl 1) + Carry;

      Carry := Value div FBase;
      FDigits[j] := Value mod FBase;
    end;
  end;

  Adjust;

  Result := Self;
end;

(*
 * 概要  右に iNum 個シフトする
 * 引数  iNum  シフトするビット数
 * 戻値  自分のコピー
 *)
function TBigInteger.ShiftR(const iNum: Cardinal): TBigInteger;
var
  Carry: Cardinal;
  LSB: Cardinal;
  Value: Cardinal;
begin
  var Len := Length(FDigits);

  for var i := 0 to iNum - 1 do
  begin
    Carry := 0;

    for var j := Len - 1 downto 0 do
    begin
      Value := Carry + FDigits[j];

      LSB := Value and 1;

      Value := Value shr 1;

      Carry := LSB * FBase;
      FDigits[j] := Value mod FBase;
    end;
  end;

  Adjust;

  Result := Self;
end;

(*
 * 概要  減算
 * 引数  iNum  減じる値
 * 戻値  自分のコピー
 *)
function TBigInteger.Sub(const iNum: TBigInteger): TBigInteger;
begin
  if (FSign = iNum.FSign) then
  begin
    if (CompareLength(iNum) < 0) then
      FSign := -iNum.FSign;

    InternalSub(iNum);
  end
  else
    InternalAdd(iNum);

  Result := Self;
end;

(*
 * 概要  減算
 * 引数  iNum  減じる値
 * 戻値  自分のコピー
 *)
function TBigInteger.Sub(const iNum: Integer): TBigInteger;
begin
  Result := Sub(TBigInteger.CreateInteger(iNum, FBase));
end;

class operator TBigInteger.Subtract(iA: TBigInteger; iB: Integer): TBigInteger;
begin
  Result := iA.Sub(iB);
end;

class operator TBigInteger.Subtract(iA, iB: TBigInteger): TBigInteger;
begin
  Result := iA.Sub(iB);
end;

end.
