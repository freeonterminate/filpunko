﻿(*
 * KUSO APPLICATION
 *
 * PLATFORMS
 *   Windows / macOS / Android / iOS
 *
 * ENVIRONMENT
 *   Delphi 10.4.1 or Lator
 *
 * LICENSE
 *   Copyright (C) 2020 HOSOKAWA Jun
 *   Released under the MIT license
 *   http://opensource.org/licenses/mit-license.php
 *
 * HISTORY
 *   2020/11/28 Version 1.0.0  Release
 *
 * Programmed by HOSOKAWA Jun (twitter: @pik)
 *)

unit uMain;

interface

uses
  System.Classes
  , System.IniFiles
  , System.Math.Vectors
  , System.SysUtils
  , System.Types
  , System.UITypes
  , FMX.Ani
  , FMX.Controls
  , FMX.Controls.Presentation
  , FMX.Controls3D
  , FMX.Forms
  , FMX.Layers3D
  , FMX.Layouts
  , FMX.Objects
  , FMX.Objects3D
  , FMX.Platform
  , FMX.StdCtrls
  , FMX.Types
  , FMX.Viewport3D
  , PK.Math.BigInteger
  ;


type
  TfrmUnko = class(TForm)
    viewportUnko: TViewport3D;
    layoutOperation: TLayout;
    gridBase: TGrid3D;
    layoutCountBase: TLayout;
    styleAir: TStyleBook;
    lblCount: TLabel;
    layserTitleBase: TLayer3D;
    layserUnkoBase: TLayer3D;
    imgUnko: TImage;
    animRotationY: TFloatAnimation;
    imgTItle: TImage;
    layoutMountainBase: TLayout;
    imgMtL: TImage;
    imgMtR: TImage;
    procedure layoutOperationMouseMove(Sender: TObject; Shift: TShiftState; X,
      Y: Single);
    procedure layoutOperationMouseUp(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure layoutOperationMouseDown(Sender: TObject; Button: TMouseButton;
      Shift: TShiftState; X, Y: Single);
    procedure FormCreate(Sender: TObject);
    procedure animRotationYProcess(Sender: TObject);
    procedure FormDestroy(Sender: TObject);
    procedure animRotationYFinish(Sender: TObject);
    procedure FormKeyUp(Sender: TObject; var Key: Word; var KeyChar: Char;
      Shift: TShiftState);
    procedure lblCountClick(Sender: TObject);
  private const
    SECTION_SYSTEM = 'system';
    IDENT_COUNT = 'count';
    INI_FILENAME = 'unko.ini';
  private var
    FBeginX: Single;
    FBeginAngleY: Single;
    FBeginTime: TDateTime;
    FPrevX: Single;
    FPrevAngleY: Single;
    FIniFile: TIniFile;
    FCount: TBigInteger;
    FIsForward: Boolean;
  private
    function CalcDX(const AX: Single): Single;
    procedure ProcessY;
    procedure SaveCount;
    function AppEvent(iAppEvent: TApplicationEvent; iContext: TObject): Boolean;
  public
  end;

var
  frmUnko: TfrmUnko;

implementation

{$R *.fmx}

uses
  System.DateUtils
  , System.Math
  , System.IOUtils
  , FMX.DialogService
  {$IFDEF ANDROID}
  , Androidapi.Helpers
  , Androidapi.JNI.App
  {$ENDIF}
  ;

procedure TfrmUnko.animRotationYFinish(Sender: TObject);
begin
  SaveCount;
end;

procedure TfrmUnko.animRotationYProcess(Sender: TObject);
begin
  ProcessY;
end;

function TfrmUnko.AppEvent(
  iAppEvent: TApplicationEvent;
  iContext: TObject): Boolean;
begin
  Result := False;

  case iAppEvent of
    TApplicationEvent.BecameActive:
    begin
      Invalidate;
    end;
  end;
end;

function TfrmUnko.CalcDX(const AX: Single): Single;
begin
  FIsForward := AX > FPrevX;
  FPrevX := AX;

  Result := FBeginX - AX
end;

procedure TfrmUnko.FormCreate(Sender: TObject);
begin
  layoutOperation.BringToFront;
  layoutOperation.AutoCapture := True;

  FIniFile :=
    TIniFile.Create(TPath.Combine(TPath.GetDocumentsPath, INI_FILENAME));

  FCount := FIniFile.ReadString(SECTION_SYSTEM, IDENT_COUNT, '0');
  lblCount.Text := FCount;

  viewportUnko.RecalcSize;

  var Service: IFMXApplicationEventService;
  if
    TPlatformServices.Current.SupportsPlatformService(
      IFMXApplicationEventService,
      Service
    )
  then
    Service.SetApplicationEventHandler(AppEvent);
end;

procedure TfrmUnko.FormDestroy(Sender: TObject);
begin
  FIniFile.Free;
end;

procedure TfrmUnko.FormKeyUp(
  Sender: TObject;
  var Key: Word;
  var KeyChar: Char;
  Shift: TShiftState);
begin
  {$IFDEF ANDROID}
  if Key = vkHardwareBack then
  begin
    Key := 0;
    KeyChar := #0;
    TAndroidHelper.Activity.MoveTaskToBack(True);
  end;
  {$ENDIF}
end;

procedure TfrmUnko.layoutOperationMouseDown(
  Sender: TObject;
  Button: TMouseButton;
  Shift: TShiftState;
  X, Y: Single);
begin
  animRotationY.Stop;

  FPrevX := X;
  FBeginX := X;
  FBeginAngleY := layserUnkoBase.RotationAngle.Y;
  FBeginTime := Now;
end;

procedure TfrmUnko.layoutOperationMouseMove(
  Sender: TObject;
  Shift: TShiftState;
  X, Y: Single);
begin
  if not layoutOperation.Pressed then
    Exit;

  var DX := CalcDX(X);
  layserUnkoBase.RotationAngle.Y := FBeginAngleY + DX;

  ProcessY;
end;

procedure TfrmUnko.layoutOperationMouseUp(
  Sender: TObject;
  Button: TMouseButton;
  Shift: TShiftState;
  X, Y: Single);
begin
  FPrevX := FBeginX;
  var DX := CalcDX(X);
  var DT := MilliSecondsBetween(Now, FBeginTime);

  FPrevAngleY := layserUnkoBase.RotationAngle.Y;

  animRotationY.StartValue := FPrevAngleY;
  animRotationY.StopValue := DX * animRotationY.Duration * 1000 / DT;

  animRotationY.Start;
end;

procedure TfrmUnko.lblCountClick(Sender: TObject);
begin
  TDialogService.MessageDialog(
    'Clear the count ?',
    TMsgDlgType.mtConfirmation,
    [TMsgDlgBtn.mbYes, TMsgDlgBtn.mbNo],
    TMsgDlgBtn.mbYes,
    0,
    procedure (const AResult: TModalResult)
    begin
      if AResult = mrYes then
      begin
        FCount := 0;
        lblCount.Text := FCount;
        SaveCount;
      end;
    end
  );
end;

procedure TfrmUnko.ProcessY;

  procedure CountUp(const ADelta: Integer);
  begin
    FCount.Add(ADelta);
    lblCount.Text := FCount;
  end;

begin
  var Y := layserUnkoBase.RotationAngle.Y;

  if (Y > 0) and (Abs(Y - FPrevAngleY) > 30) then
  begin
    if FIsForward then
    begin
      if (Y > FPrevAngleY) then
        CountUp(1);
    end
    else
    begin
      if (Y < FPrevAngleY) then
        CountUp(-1);
    end;

    FPrevAngleY := Y;
  end;
end;

procedure TfrmUnko.SaveCount;
begin
  FIniFile.WriteString(SECTION_SYSTEM, IDENT_COUNT, FCount);
  FIniFile.UpdateFile;
end;

end.
