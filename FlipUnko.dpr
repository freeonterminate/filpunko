﻿program FlipUnko;

uses
  System.StartUpCopy,
  FMX.Forms,
  FMX.Types,
  uMain in 'uMain.pas' {frmUnko},
  PK.Math.BigInteger in 'PK.Math.BigInteger.pas';

{$R *.res}

begin
  {$IFDEF DEBUG}
  ReportMemoryLeaksOnShutdown := True;
  {$ENDIF}

  Application.Initialize;
  Application.CreateForm(TfrmUnko, frmUnko);
  Application.Run;
end.
